



println("Checking result")



function f(x)
	let out = 0
		for t in 1:n_T, g in 1:n_G
			p_tg = value(p[t, g])
			out += df[g, :A] * p_tg^2 + df[g, :B]*p_tg + df[g, :C] + df[g, :D]*abs(sin(df[g, :E]*(p_tg-df[g, :P_min])))
		end
	return out
	end
end

println("True objective is ", f(p))

total_dev = 0
let dev = 0, losses = 0
	for t in 1:n_T
		dev = dev + abs(value(sum_gen[t])  - D[t, :D] - value(p_loss[t]))
		losses = losses + value(p_loss[t])

	end

	println("Balance deviation ", dev)
	println("Losses ", losses)
end

using JuMP
using Ipopt

include("getData.jl")

model = Model();

set_optimizer(model, Ipopt.Optimizer);
#set_optimizer_attribute(model, "max_iter",  100)

(n_T, _) = size(T) 
(n_G, _) = size(G) 



@variable(model, p[1:n_T, 1:n_G])

@constraint(model, power_range[t = 1:n_T, g = 1:n_G], df[g, :P_min] <= p[t, g] <= df[g, :P_max])
@constraint(model, ramp_rate[t = 2:n_T, g = 1:n_G], -df[g, :R_down] <= p[t, g] - p[t-1, g] <= df[g, :R_up])


@expression(model, sum_gen[t = 1:n_T], sum(p[t, g] for g in 1:n_G))
@expression(model, p_loss[t = 1:n_T], sum(p[t, i] * B_loss[i, j] * p[t, j] for i in 1:n_G, j in 1:n_G) + sum(p[t, i] * B_loss_0[i, 1] for i in 1:n_G))
#@expression(model, p_loss[t = 1:n_T], p_loss_quad[t] + p_loss_lin[t] + p_loss_cons[t])

@constraint(model, balance[t = 1:n_T], sum_gen[t] == D[t, :D] + p_loss[t])

@expression(model, arg_sin[t = 1:n_T, g = 1:n_G], df[g, :E] * (p[t, g] - df[g, :P_min]))

@variable(model, aux_sin[1:n_T, 1:n_G])
@constraint(model, cons_sin[t = 1:n_T, g = 1:n_G], aux_sin[t, g] == df[g, :E] * (p[t, g] - df[g, :P_min]))

@variable(model, aux_abs[1:n_T, 1:n_G])
@NLconstraint(model, cons_abs[t = 1:n_T, g = 1:n_G], aux_abs[t, g] == sin(aux_sin[t, g]))


@expression(model, f_Q_foo[t = 1:n_T, g = 1:n_G], df[g, :A] * p[t, g]^2 + df[g, :B]*p[t, g] + df[g, :C])

#@expression(model, f_S[t = 1:n_T, g = 1:n_G], df[g, :G]*abs(aux_abs[t, g])) 


@variable(model, f_S[1:n_T, 1:n_G])
@NLconstraint(model, test_abs[t = 1:n_T, g = 1:n_G], f_S[t, g] == df[g, :D]*abs(aux_abs[t, g]))

@variable(model, f_Q[1:n_T, 1:n_G])
@constraint(model, foo[t = 1:n_T, g = 1:n_G], f_Q[t, g] == f_Q_foo[t, g])



@NLobjective(model, Min, sum(f_Q[t, g] + f_S[t, g] for t in 1:n_T, g in 1:n_G))

optimize!(model)

include("checkResult.jl")



using CSV
using DataFrames

file_path="../data/tmp/"
println(file_path)

function load_CSV(file_name::AbstractString, remove_first::Bool=false)
	rel_path = string(file_path, file_name)
	df = DataFrame!(CSV.File(rel_path; header=false))
	if remove_first
		df = select(df, Not(:Column1))
	end
	return df
end

""" Losses """


B_loss = load_CSV("Bloss.csv")
println(B_loss)
print(B_loss[1, :Column1])

B_loss_0 = load_CSV("Bloss_0.csv")
println(B_loss_0)
println(B_loss_0[1, 1])

B_loss_00 = load_CSV("Bloss_00.csv")
println(B_loss_00[1,1])

""" Generators """
G = load_CSV("Generators.csv")
rename!(G, ["G"])
println("Generators", G)

df = G



""" Generator Cost Function """ 

P = load_CSV("GeneratorsCostFun.csv", true)
rename!(P, ["A", "B", "C", "D", "E"])

df[:"A"] = P[:"A"]
df[:"B"] = P[:"B"]
df[:"C"] = P[:"C"]
df[:"D"] = P[:"D"]
df[:"E"] = P[:"E"]

print(df)



""" Demand """

rel_path = string(file_path, "Demand.csv")
D = DataFrame!(CSV.File(rel_path; transpose=true, skipto=2, header=false))

(_, n_L) = size(D)

D_arr = [sum(D[i, :]) for i in 1 : size(D,1)]

D = DataFrame()

D["D"] = D_arr

println(D)

println(size(D))



""" P_max """ 

P_max = load_CSV("MaxRunCapacity.csv", true)
rename!(P_max, ["P_max"])
println(P_max)
df[:"P_max"] = P_max[:"P_max"]

""" P_min """ 

P_min = load_CSV("MinRunCapacity.csv", true)
rename!(P_min, ["P_min"])
println(P_min)
df[:"P_min"] = P_min[:"P_min"]

""" R_down """ 

R_down = load_CSV("RampDown.csv", true)
rename!(R_down, ["R_down"])
df[:"R_down"] = R_down[:"R_down"]
println(R_down)

""" R_up """ 

R_up = load_CSV("RampUp.csv", true)
rename!(R_up, ["R_up"])
println(R_up)
df[:"R_up"] = R_up[:"R_up"]

""" TimeStep """
T = load_CSV("TimeStep.csv")
rename!(T, ["T"])
println("T", T)

println(P)
println(R_up)

println(df)

println(B_loss)
println(size(B_loss))

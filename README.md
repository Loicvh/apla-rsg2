# APLA-RSG

APLA-RSG is a Python library for solving non-convex and non-smooth power dispatches. The idea is to build a sequence of adaptive piecewise-linear approximation (APLA) of the objective and then improve the solution through a local search via a Riemannian subgradient descent..

## Installation

Simply clone the repository on your computer.

```
git clone https://gitlab.com/Loicvh/apla-rsg
```

## Dependencies

This repository depends on

- Multiple python libraries: numpy, csv, os, ...

- Python Gurobi API, install it [here](https://www.gurobi.com/documentation/8.1/refman/py_python_api_overview.html). Free academic license can be obtained on their website.

- Julia (JuMP) with Ipopt.

## Usage

Up to now the usage is strictly limited to UNIX system. This is mainly due to the way other OS manage files.

To run the full tests simply enters:

```
sh run_APLA_RSG.sh
```
The test case can be chosen by changing ```DATA_TYPE``` inside ```run.sh```.


## Problem Definition
The problem of interest here is the economic dispatch which consists in dispatching the electrical demand among the commited power unit in order to meeth the system load at minimal cost. The objective function is subjected to operational constraints such as the min and max power range, ramping constraints. The consideration of the power losses makes the feasible set inherently non-convex, as it consists in a quadratic hypersurface.


## Method Description

The first part of the algorithm, APLA, is initially described in [1]. The extension of APLA to account the power losses is made in [2].


## Contributing
Pull requests, comments and questions are welcome. 

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Sources
[1]. (2020) L. Van Hoorebeeck, P.-A. Absil and A. Papavasiliou, “Global Solution of Economic Dispatch with Valve Point Effects and Transmission Constraints”, Electric Power Systems Research, Volume 189, 2020, ISSN 0378-7796.

[2]. (2021) L. Van Hoorebeeck, P.-A. Absil and A. Papavasiliou, “Solving non-convex economic dispatch with valve-point effect and losses with guaranteed accuracy”, preprint.

#! python3

# Global import
from gurobipy import *
import numpy as np
import bisect
import time
import os
import csv
from eldp import Eldp
from eldp import Eldp_R
from eldp import Param


# Local import
import fun

alg_dflt = 'APLA'


eps_p = pow(10, -6)
root_dir, _ = os.path.split(os.path.dirname(os.path.abspath(__file__)))


def problem(E, param):
    # PROBLEM(alg=alg_dflt) creates an optimization model of the economic dispatch
    #
    # return:   mod: the model variable of gurobipy
    #           gen_VPE: set of generator obeying a VPE
    #           x_kink: set of kink point
    #           xi: set of knots
    #           P: parameters, see getP
    #           p: gurobi variable
    #           p_VPE: gurobi variable
    #
    # This function uses Gurobi Python API with data obtained through getData.
    # A valid gurobi license is needed. Three algorithms are supported:
    # APLA (Adaptive Piecewise Linear Approximation)
    # APQUA (Adaptive Piecewise-Quadratic Under-Approximation)
    # QP_no_VPE (Quadratic Programming while ignoring the VPE)


    mod = Model()
    mod.Params.mipgap = param.mipgap
    data = E.data
    var = E.var
    var.p = mod.addVars(data.gen_T, lb=E.p_min_T, ub=E.p_max_T, name='p')
    # p_VPE is useless and trivially equals to 0 for alg=(APLA or QP_no_VPE)
    var.p_VPE = mod.addVars(data.gen_VPE_T, lb=E.p_min_T, ub=E.p_max_T, name='p_VPE')
    var.f = mod.addVars(data.lines_T, lb=data.TC_min_T, ub=data.TC_max_T, name='f')
    var.theta = mod.addVars(data.bus_T, name='theta')
    if E.param.flag_OPF:
        for (t, b) in data.bus_T:
            mod.addConstr(
                data.D_tot[t, b] == sum(var.f[t, l] for l in data.lines if data.from_bus[l] == b)
                - sum(var.f[t, l] for l in data.lines if data.to_bus[l] == b)
                + sum(var.p[t, g] for g in data.gen if data.bus_generator[g] == b),
                name='FlowConservation[%s,%s]' % (t, b))

        for (t, k) in data.lines_T:
            mod.addConstr(
                var.f[t, k] - data.B[k]*(var.theta[t, data.from_bus[k]]
                                         - var.theta[t, data.to_bus[k]]) == 0,
                name='Susceptance[%s,%s]' % (t, k))
    else:
        for t in data.T:
            mod.addConstr(
                data.D_full[data.T_dic[t]] == sum(var.p[t, g] for g in data.gen),
                    name='FlowConservation[%s,B1]' % t) 

    if len(data.T) > 1:
        for t_idx in range(1, len(data.T)):
            t_now = data.T[t_idx]
            t_before = data.T[t_idx-1]
            for g in data.gen:
                mod.addConstr(-data.RD[g] <= var.p[t_now, g] - var.p[t_before, g],
                              name='RampDown')
                mod.addConstr(var.p[t_now, g] - var.p[t_before, g] <= data.RU[g],
                              name='RampUp')

    cumSum = 0
    for g in data.gen_VPE:
        if len(E.xi[data.T[0], g]) == 1:
            cumSum += E.f(data.p_min[g], data.T[0], g)

    if param.alg == 'APLA':
        mod.setObjective(
            len(data.T)*cumSum
            + quicksum(
                (data.gen_fun_param[g][0]*var.p[t, g]*var.p[t, g]
                 + data.gen_fun_param[g][1]*var.p[t, g]
                 + data.gen_fun_param[g][2])
                for (t, g) in data.gen_T if g not in data.gen_VPE),
            GRB.MINIMIZE)
        updatePWL(E, mod, param)
    elif param.alg == 'APQUA':
        # Gurobi API does not allow a 'usual' objective along with a piecewise one.
        # Hence we use two identical variable p and p_vpe.
        mod.addConstrs(var.p[t, g] == var.p_VPE[t, g] for (t, g) in data.gen_VPE_T)
        mod.setObjective(quicksum(
                             (data.gen_fun_param[g][0]*var.p[t, g]*var.p[t, g]
                              + data.gen_fun_param[g][1]*var.p[t, g]
                              + data.gen_fun_param[g][2])
                             for (t, g) in data.gen_T),
                         GRB.MINIMIZE)
        updatePWL(E, mod, param)
    elif param.alg == 'APQUA-Multi':
        # Gurobi API does not allow a 'usual' objective along with a piecewise one.
        # Hence we use two identical variable p and p_vpe.
        mod.addConstrs(var.p[t, g] == var.p_VPE[t, g] for (t, g) in data.gen_VPE_T)
        mod.setObjective(quicksum(
            (data.gen_fun_param[g][0]*var.p[t, g]*var.p[t, g]
             + data.gen_fun_param[g][1]*var.p[t, g]
             + data.gen_fun_param[g][2])
            for (t, g) in data.gen_T)
            + quicksum(1*
                E.ell.A_full[t][i, j]*var.p[t, data.gen[i]]*var.p[t, data.gen[j]]
                for i in range(data.n) for j in range(data.n) for t in data.T)
            + quicksum(1*
                (E.ell.b_full[t][i]+1)*var.p[t, data.gen[i]]
                for i in range(data.n) for t in data.T), GRB.MINIMIZE)
        updatePWL(E, mod, param)
    elif param.alg == 'QP_no_VPE':
        mod.setObjective(quicksum(
            (data.gen_fun_param[g][0]*var.p[t, g]*var.p[t, g]
             + data.gen_fun_param[g][1]*var.p[t, g]
             + data.gen_fun_param[g][2])
            for (t, g) in data.gen_T),
            GRB.MINIMIZE)
    else:
        raise ValueError('Error algorithm %s not known', param.alg)

    return mod


def solve_problem(E, mod, param):
    # SOLVEPROBLEM(alg=alg_dflt, n_iter=10, flag_write) solves the problem using the algorithm
    #                                                   alg with n_iter max iteration and
    #                                                   save results in /tmp if flag_write.
    #
    # return best_obj, gen_VPE, m, mod, p, p_VPE, P, x_kink, xi
    # return:   best_obj: the best true objective
    #           gen_VPE: set of generator obeying a VPE
    #           m: length of knots
    #           mod: the model variable of gurobipy
    #           p: gurobi variable
    #           p_VPE: gurobi variable
    #           P: parameters, see getP
    #           x_kink: set of kink point
    #           xi: set of knots
    #
    # This function uses Gurobi Python API to solve the problem defined with getProblem.
    # A valid gurobi license is needed. Three algorithms are supported:
    # APLA (Adaptive Piecewise Linear Approximation)
    # APQUA (Adaptive Piecewise-Quadratic Under-Approximation)
    # QP_no_VPE (Quadratic Programming while ignoring the VPE)

    tic_solve_problem = time.time()
    data = E.data
    var = E.var


    k = 0
    flagInsert = True
    mod.Params.timeLimit = param.timeLimit
    if param.alg == 'QP_no_VPE':
        print('QP no VPE algorithm')
        mod.optimize()
        k = param.n_iter  # to be sure to not go in the loop
        E.best_sol = mod.getAttr('x', var.p)
        E.best_obj = E.f(E.best_sol)
        E.best_bound = mod.objVal
        return mod
    else:
        if param.flag_warm_start:
            # Warm start
            param.flag_warm_start = False
            mod_QP = solve_problem(E, mod, param)
            mod_QP.update()
            for (t, g) in data.T_gen:
                var.p[t, g].Start = E.best_sol[t, g]
    E.delta_loc = param.delta_tol+1
    while k < param.n_iter and flagInsert and E.delta_loc > param.delta_tol:
        print('\n \n ************************* \n Solve Problem iteration number %d \n ************************* \n\n' % k)
        mod.update()
        if E.isSlave:
            print('Feeding with current incumbent', k, E.incumbent_num)
            for (t, g) in data.gen_T:
                if k == 0:  # Feeding with current incumbent
                    var.p[t, g].Start = E.incumbents_sol[E.incumbent_num][t, g]
                    #     else:  # Feeding with previous solution
                    #         var.p[t, g].Start = sol[t, g]
        mod.write('to_todelte.lp')
        mod.optimize()
        print('Mod status is', mod.status)
        if mod.status == 6 and E.isSlave:  # 6=timelimite
            return
        sol = mod.getAttr('x', var.p)
        E.true_obj = E.f(sol)
        E.update_best_sol(sol)
        print(E.best_bound)
        if mod.isMIP and not E.isSlave:
            E.update_best_bound(mod.ObjBound)

        E.sur_obj = mod.objVal
        n_solutions = min(mod.SolCount, param.max_sol)
        print('Number of solutions found: ' + str(n_solutions))
        for e in range(1, n_solutions):
            mod.setParam(GRB.Param.SolutionNumber, e)
            _sol = mod.getAttr('Xn', var.p)
            E.update_best_sol(_sol)
        mod.setParam(GRB.Param.SolutionNumber, 0)
        # Updating knots
        flagInsert = False
        nInsort = 0
        for t in data.T:
            for g in data.gen_VPE:
                idx = insortTol(E.xi[t, g], sol[t, g], eps_p)  # Insortion made here if needed!
                if idx > -1:
                    nInsort += 1
                    flagInsert = True
        print("Total  number of insortion is", nInsort)
        print("Flag insertion is", flagInsert)
        E.printBest()
        updatePWL(E, mod, param)
        k = k+1
    E.printBest()
    
    if param.flag_write:
        # real_bound and exec_time seem useless but are used in
        # myfile.write(str(eval(var))) !
        real_bound = E.best_bound
        best_obj = E.best_obj
        opt_gap = E.delta/best_obj*100.
        exec_time = time.time() - tic_solve_problem
        dirs = ['real_bound', 'best_obj', 'opt_gap', 'exec_time']
        if param.alg == 'QP_no_VPE':
            pass
            #  dirs.append('percentage')
        for var in dirs:
            with open(os.path.join(root_dir, "tmp/", param.alg,
                                   "case" + str(len(data.bus)) + "_" + str(var) + ".txt"),
                      "a") as myfile:
                myfile.write(str(eval(var))+"\n")
        str_sol = 'sol'+str(data.n)+'_'+str(len(data.T))+'.txt'
        np.savetxt(os.path.join(root_dir, 'tmp/', str_sol), E.best_sol.values())
    return mod


def master(E, param, mod=None):
    # MASTER solves the problem over the whole feasible set. Then, it uses SLAVE to enhance
    # the previously obtained incumbents via a local approach.

    tic_master = time.time()
    root_dir, _ = os.path.split(os.path.dirname(os.path.abspath(__file__)))
    data = E.data
    var = E.var
    if not mod:
        mod = problem(E, param)
    mod.params.PoolSolutions = param.max_sol
    solve_problem(E, mod, param)
    n_solutions = min(mod.SolCount, param.max_sol)
    E.incumbents_sol = [0]*n_solutions
    print('Number of solutions found: ' + str(n_solutions))
    for e in range(0, n_solutions):
        mod.setParam(GRB.Param.SolutionNumber, e)
        sol = mod.getAttr('Xn', var.p)
        E.incumbents_sol[e] = sol
        E.update_best_sol(sol)
#    for e in range(0, n_solutions):
#        p_print = np.array(p_list[e])
#        (_, n) = p_print.shape
#        str_sol = 'sol'+str(n)+'_'+str(len(T))+'_'+str(e)+'.txt'
#        np.savetxt(os.path.join(root_dir, 'tmp/', str_sol), p_print) 

    for e in range(n_solutions):
        E.incumbent_num = e
        print(var.p['T1', 'G1'].lb)
        print(var.p['T1', 'G1'].ub)
        restrict_range(E, mod, param)
        updatePWL(E, mod, param)
        print(var.p['T1', 'G1'].lb)
        print(var.p['T1', 'G1'].ub)

        # mod.Params.Cutoff = best_obj
        
        for (t, g) in data.gen_T:
            var.p[t, g].Start = E.incumbents_sol[e][t, g]

        mod.update()
        print('Entering Slave number', e)
        E.isSlave = True
        #E.reset_xi()
        mod.Params.Cutoff = E.best_obj
        mod.write(os.path.join(root_dir, 'tmp', 'gurobi', 'slave.lp'))
        param.n_iter = 20
        param.timeLimit = 10
        solve_problem(E, mod, param)
        print('Best obj after slave', E.best_obj)
    exec_time = time.time() - tic_master # /! used below
    #for var in ['real_bound', 'best_obj', 'opt_gap', 'exec_time']:
    #    with open(os.path.join(root_dir,"tmp/", string_file+'-'+restrict_method, "case"+str(len(bus))+"_"+str(var)+".txt"), "a") as myfile:
    #        myfile.write(str(eval(var))+"\n")
    #(T, n) = best_sol.shape
    #str_sol = 'sol'+str(n)+'_'+str(T)+'.txt'
    #np.savetxt(os.path.join(root_dir, 'tmp/', str_sol), best_sol) 


def restrict_range(E, mod, param):
    if param.restrict_method == 'local':
        restrict_range_local(E, mod, param)
    elif param.restrict_method == 'full':
        restrict_range_full(E, mod, param)
    else:
        raise ValueError('Invalid restrict_method argument: restrict_method\
                         should be local or full, current value is', param.restrict_method)


def restrict_range_local(E, mod, param):
    data = E.data
    var = E.var
    p_inc = E.incumbents_sol[E.incumbent_num]
    for g in data.gen_VPE:
        _m = len(E.x_kink[g])
        for t in data.T:
            for j in range(1, _m):
                if p_inc[t, g] < E.x_kink[g][j]:
                    var.p[t, g].ub = E.x_kink[g][j]
                    var.p[t, g].lb = E.x_kink[g][j-1]
                    break
                elif abs(p_inc[t, g] - E.x_kink[g][j]) <= param.eps_p and j < _m-1:
                    var.p[t, g].ub = E.x_kink[g][j+1]
                    var.p[t, g].lb = E.x_kink[g][j-1]
                    break
                elif j == _m-1:
                    var.p[t, g].lb = E.x_kink[g][j-1]
                    var.p[t, g].ub = E.x_kink[g][j]


def restrict_range_full(E, mod, param):
    data = E.data
    var = E.var
    p_inc = E.incumbents_sol[E.incumbent_num]
    for g in data.gen_VPE:
        _m = len(E.x_kink[g])
        for t in data.T:
            for j in range(1, _m-1):
                if j == _m-2:
                    var.p[t, g].lb = E.x_kink[g][j-1]
                    var.p[t, g].ub = E.x_kink[g][j+1]
                    break
                elif p_inc[t, g] < E.x_kink[g][j]:
                    var.p[t, g].ub = E.x_kink[g][j+1]
                    var.p[t, g].lb = E.x_kink[g][j-1]
                    break
                elif abs(p_inc[t, g] - E.x_kink[g][j]) <= param.eps_p and j < _m-1:
                    var.p[t, g].ub = E.x_kink[g][j+1]
                    var.p[t, g].lb = E.x_kink[g][j-1]
                    break


def updatePWL(E, mod, param):
    data = E.data
    var = E.var
    for g in data.gen_VPE:
        for t in data.T:
            if len(E.xi[t, g]) > 1:
                if param.alg == 'APLA':
                    mod.setPWLObj(var.p[t, g], E.xi[t, g], E.f(np.asarray(E.xi[t, g]), t, g))
                elif param.alg == 'APQUA':
                    mod.setPWLObj(var.p_VPE[t, g], E.xi[t, g],
                                  fun.f_VPE_part(np.asarray(E.xi[t, g]), data.gen_fun_param[g],
                                                data.p_min[g]))


def insortTol(vec, x, eps_p):
    index = bisect.bisect_left(vec, x)
    if index == 0 or index == len(vec):
        return -1
    if x-vec[index-1] < eps_p or vec[index] - x < eps_p:
        return -1
    else:
        vec.insert(index, x)
    return index


def test_solve_problem():
    tic = time.time()
    solve_problem()
    toc = time.time() - tic
    print('Elapsed time is', toc)


def test_master(string_file='master', method='local'):
    tic = time.time()
    master(string_file, method)
    toc = time.time() - tic
    print('Elapsed time is', toc)


def main():
    E = Eldp_R()
    data = E.data
    full_D = np.zeros((len(E.data.T), 1))
    i = 0
    for (t, b) in E.data.bus_T:
        full_D[E.data.T_dic[t]] += data.D_tot[t, b]
    param = Param(alg='APLA')
    E.param.flag_OPF = False
    param.n_iter = 1
    param.flag_warm_start = False
    param.restrict_method = 'local'
    mod = problem(E, param)
    mod.write('todelete.lp')
    solve_problem(E, mod, param)
    print('APQUA test')
    param.alg = 'APLA'
    solve_problem(E, param)
    master(E, param)

if __name__ == "__main__":
    main()

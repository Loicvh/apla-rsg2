#! python3

import csv
import os

import numpy as np

flag_VPE = False


root_dir, _ = os.path.split(os.path.dirname(os.path.abspath(__file__)))

str_data_path_dflt = os.path.join(root_dir, 'data/tmp/')
str_bus_file_dflt = 'Buses.csv'
str_Bloss_file_dflt = 'Bloss.csv'
str_Bloss_0_file_dflt = 'Bloss_0.csv'
str_Bloss_00_file_dflt = 'Bloss_00.csv'
str_busgenerator_file_dflt = 'BusGenerators.csv'
str_busload_file_dflt = 'BusLoads.csv'
str_demand_file_dflt = 'Demand.csv'
str_frombus_file_dflt = 'FromBus.csv'
str_generator_file_dflt = 'Generators.csv'
str_generatorcostfun_file_dflt = 'GeneratorsCostFun.csv'
str_line_file_dflt = 'Lines.csv'
str_load_file_dflt = 'Loads.csv'
str_maxruncapacity_file_dflt = 'MaxRunCapacity.csv'
str_minruncapacity_file_dflt = 'MinRunCapacity.csv'
str_test_obj_dflt = 'test_obj.csv'
str_test_x0_dflt = 'test_x0.csv'
str_rampdown_file_dflt = 'RampDown.csv'
str_rampup_file_dflt = 'RampUp.csv'
str_susceptance_file_dflt = 'Susceptance.csv'
str_TC_file_dflt = 'TC.csv'
str_TC_file_dflt = 'TC_nonzero.csv'
print('Warning! Not using Matpower line constraints')
str_test_sol_dflt = 'test_sol.csv'
str_timestep_file_dflt = 'TimeStep.csv'
str_tobus_file_dflt = 'ToBus.csv'

if flag_VPE:
    str_busgenerator_file_dflt = 'BusGenerators_VPE.csv'
    str_generator_file_dflt = 'Generators_VPE.csv'
    str_generatorcostfun_file_dflt = 'GeneratorsCostFun_VPE.csv'
    str_maxruncapacity_file_dflt = 'MaxRunCapacity_VPE.csv'
    str_minruncapacity_file_dflt = 'MinRunCapacity_VPE.csv'


def getData():
    Bloss = getBloss()
    Bloss_0 = getBloss_0()
    Bloss_00 = getBloss_00()
    bus = getBuses()
    bus_generator = getBusGenerator()
    bus_load = getBusLoad()
    D = getDemand()
    from_bus = getFromBus()
    gen = getGenerators()
    gen_fun_param = getGeneratorsCostFun()
    lines = getLines()
    loads = getLoads()
    p_max = getMaxRunCapacity()
    p_min = getMinRunCapacity()
    B = getSusceptances()
    RD = getRampDown()
    RU = getRampUp()
    SR = getSpinningReserve()
    T = getTimeStep()
    TC = getTC()
    n = getN()

    to_bus = getToBus()
    return B, Bloss, Bloss_0, Bloss_00, bus, bus_generator, bus_load, D, from_bus, gen,\
            gen_fun_param, lines, loads, n, p_max, p_min, RD, RU, SR, T, TC, to_bus


def getN(str_bus=str_data_path_dflt + str_generator_file_dflt):
    return len(getGenerators(str_bus))


def test_getN():
    print('\n Number of generator is', getN())


def get_test_obj(str_test_obj_sfx=str_test_obj_dflt):
    str_test_obj = str_data_path_dflt+str_test_obj_sfx
    return np.genfromtxt(str_test_obj, delimiter=',')


def get_test_sol(str_test_sol_sfx=str_test_sol_dflt):
    str_test_sol = str_data_path_dflt+str_test_sol_sfx
    return to_dic(np.genfromtxt(str_test_sol, delimiter=','))


def get_test_x0(str_test_x0=str_data_path_dflt + str_test_x0_dflt):
    return to_dic(np.genfromtxt(str_test_x0, delimiter=',').T)


def getBloss(str_Bloss=str_data_path_dflt + str_Bloss_file_dflt):
    if not os.path.exists(str_Bloss):
        return np.zeros((getN(), getN()))
    return np.genfromtxt(str_Bloss, delimiter=',')


def getBloss_0(str_Bloss_0=str_data_path_dflt + str_Bloss_0_file_dflt):
    if not os.path.exists(str_Bloss_0):
        return np.zeros((getN(),))
    return np.genfromtxt(str_Bloss_0, delimiter=',')


def getBloss_00(str_Bloss_00=str_data_path_dflt + str_Bloss_00_file_dflt):
    if not os.path.exists(str_Bloss_00):
        return 0
    return np.genfromtxt(str_Bloss_00, delimiter=',')


def test_getBloss():
    print('\n Loss matrix is \n', getBloss())


def getBuses(str_bus=str_data_path_dflt + str_bus_file_dflt):
    Bus = []
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            Bus.append(row[0])
    return Bus


def test_getBuses():
    print('\n List of buses is \n', getBuses())


def getDemand(str_bus=str_data_path_dflt + str_demand_file_dflt, str_TS=str_data_path_dflt
              + str_timestep_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        D = {}
        T = getTimeStep(str_TS)
        for row in reader:
            t_num = 1
            for t in T:
                D[(t, row[0])] = float(row[t_num])
                t_num += 1
    return D


def test_getDemand():
    print('\n List of Demand at each bus is \n', getDemand())


def getBusGenerator(str_bus=str_data_path_dflt + str_busgenerator_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        BusGen = {row[0]: row[1] for row in reader}
    return BusGen


def test_getBusGenerator():
    print('\n List of generator with bus is \n', getBusGenerator())


def getBusLoad(str_bus=str_data_path_dflt + str_busload_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        BusLoad = {row[0]: row[1] for row in reader}
    return BusLoad


def test_getBusLoad():
    print('\n List of bus load is\n ', getBusLoad())


def getFromBus(str_bus=str_data_path_dflt + str_frombus_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        FromBus = {row[0]: row[1] for row in reader}
    return FromBus


def test_getFromBus():
    print('\n List of exiting line linked to bus is \n', getFromBus())


def getGenerators(str_bus=str_data_path_dflt + str_generator_file_dflt):
    Gen = []
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            Gen.append(row[0])
    return Gen


def test_getGenerators():
    print('\n List of Generators is \n', getGenerators())


def getGeneratorsCostFun(str_bus=str_data_path_dflt + str_generatorcostfun_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        gen_cost = {row[0]: [float(row[i]) for i in range(1, 6)] for row in reader}
    return gen_cost


def test_getGeneratorsCostFun():
    print('\n List of Generators Cost fun is \n', getGeneratorsCostFun())


def getLines(str_bus=str_data_path_dflt + str_line_file_dflt):
    Lin = []
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            Lin.append(row[0])
    return Lin


def test_getLines():
    print('\n List of Lines is \n', getLines())


def getLoads(str_bus=str_data_path_dflt + str_load_file_dflt):
    Load = []
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            Load.append(row[0])
    return Load


def test_getLoads():
    print('\n List of Loads is \n', getLoads())


def getMaxRunCapacity(str_bus=str_data_path_dflt + str_maxruncapacity_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        Pmax = {row[0]: float(row[1]) for row in reader}
    return Pmax


def test_getMaxRunCapacity():
    print('\n List of max generator capacity is \n', getMaxRunCapacity())


def getMinRunCapacity(str_bus=str_data_path_dflt + str_minruncapacity_file_dflt):
    print(str_minruncapacity_file_dflt)
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        Pmin = {row[0]: float(row[1]) for row in reader}
    return Pmin


def getRampDown(str_bus=str_data_path_dflt + str_rampdown_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        RD = {row[0]: float(row[1]) for row in reader}
    return RD


def test_getRampDown():
    print('\n Down-Ramping parameters are \n', getRampDown())


def getRampUp(str_bus=str_data_path_dflt + str_rampup_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        RU = {row[0]: float(row[1]) for row in reader}
    return RU


def test_getRampUp():
    print('\n Up-Ramping parameters are \n', getRampUp())


def test_getMinRunCapacity():
    print('\n List of min generator capacity is \n', getMinRunCapacity())


def getSpinningReserve():
    D = getDemand()
    T = getTimeStep()
    loads = getLoads()
    SR = {}
    for t in T:
        for l in loads:
            SR[t] = 0*D[t, l]
    return SR


def getSusceptances(str_bus=str_data_path_dflt + str_susceptance_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        B = {row[0]: float(row[1]) for row in reader}
    return B


def test_getSusceptances():
    print('\n List susceptance is \n', getSusceptances())


def getTimeStep(str_bus=str_data_path_dflt + str_timestep_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        T = []
        for row in reader:
            T.append(row[0])
        return T


def test_getTimeStep():
    print('\n Number of time step is', getTimeStep())


def getTC(str_bus=str_data_path_dflt + str_TC_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        TC = {row[0]: float(row[1]) for row in reader}
    return TC


def test_getTC():
    print('\n List transmission constraints is \n', getTC())


def getToBus(str_bus=str_data_path_dflt + str_tobus_file_dflt):
    with open(str_bus, 'r') as csvfile:
        reader = csv.reader(csvfile)
        ToBus = {row[0]: row[1] for row in reader}
    return ToBus


def test_getToBus():
    print('\n List of entering line linked to bus is \n', getToBus())


def to_dic(x):
    T = getTimeStep()
    return {T[t_num]: x[t_num, :] for t_num in range(len(T))}


def test():
    getData()
    print(getData())
    test_getBuses()
    test_getBloss()
    test_getBusGenerator()
    test_getBusLoad()
    test_getDemand()
    test_getFromBus()
    test_getGenerators()
    test_getLines()
    test_getLoads()
    test_getMaxRunCapacity()
    test_getMinRunCapacity()
    test_getRampDown()
    test_getRampUp()
    test_getSusceptances()
    test_getTimeStep()
    test_getTC()
    test_getToBus()
    test_getGeneratorsCostFun()
    test_getN()


def main():
    test()


if __name__ == "__main__":
    main()

import numpy as np
import math


eps_p = pow(10, -6)


def f(x, par, p_min):
    a, b, c, d, e = splitPar(par)
    return a*np.power(x, 2)+b*x+c+d*abs(np.sin(e*(x-p_min)))


def f_VPE_part(x, par, p_min):
    a, b, c, d, e = splitPar(par)
    return d*abs(np.sin(e*(x-p_min)))


def f_QP_part(x, par, p_min):
    a, b, c, d, e = splitPar(par)
    return a*pow(x, 2)+b*x+c


def df(x, par, p_min):
    a, b, c, d, e = splitPar(par)
    df_quad = 2*a*x+b
    df_VPE = -d*e*np.cos(e*(p_min-x))*np.sign(np.sin(e*(p_min-x)))
    return df_quad, df_VPE


def sg_f(x, par, idx):
    """
        x: numpy array (m, )
        idx: numpy array (n, )

    """
    a, b, c, d, e = splitPar(par)
    out = -d[idx]*e[idx] * np.eye(x.size)[:, idx]
    return out


def splitPar(par):
    return par[0], par[1], par[2], par[3], par[4]


def getKinkMid(par, p_min, p_max, s=2):
    a, b, c, d, e = splitPar(par)
    p = []
    k = 0
    if e == 0 or d == 0:
        p = [p_min, p_max]
    else:
        while k == 0 or p[k-1] < p_max:
            new_p = p_min + k*math.pi/(s*e)
            p.append(new_p)
            k = k+1
            if abs(math.sin(e*(new_p-p_min))) > eps_p:
                pass
        p[-1] = p_max

    return p

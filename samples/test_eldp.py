import numpy as np
from eldp import Eldp
from eldp import Eldp_R
from eldp import Param
import getData
import getProblem
import time
import solve


E2 = Eldp_R()
#E2.data.B_loss = np.asarray([[0.552, 0.062, -0.046], [0.062, 0.253, 0.064],
#                             [-0.046, 0.064, 0.286]]) * 0.001
#E2.data.B_loss_0 = np.array([0.0046, 0.0035, 0.0019])
#E2.data.B_loss_00 = 0.057711
E2.ell.update_ellipsoid(None)
#E2.data.T = ['T1', 'T2']
data = E2.data
E2.param.flag_OPF = False

def get_p_test():
    data = E2.data
    p_test = {}
    for t in data.T:
        p_test[t] = (data.p_max_np + data.p_min_np)/2.0
    return p_test

p_test = get_p_test()

def test_balance():
    p_test = get_p_test()
    # print(E2.balance(p_test))
    # TODO
    # assert abs(E2.balance(p_test) - 418.89) <= 0.1, 'Incorrect balance'

def test_changing_B():
    E = Eldp()
    data = E.data
    data.B = 42
    assert (data.B == 42), 'Changing data incorrectly propagated'

def test_changing_bounds():
    E = Eldp()
    data = E.data
    E.best_obj = 42
    E.best_bound = 12
    assert (E.delta == 30), 'Delta incorrectly propagated obj = %d, bound = %d but delta\
 = %d' % (E.best_obj, E.best_bound, E.delta)
    E.best_bound = 2
    assert (E.delta == 40), 'Delta incorrectly propagated obj = %d, bound = %d but delta\
 = %d' % (E.best_obj, E.best_bound, E.delta)


def test_compute_ellipsoid():
    p_test = get_p_test()
    C_dic = {t: np.array([]) for t in E2.data.T}
    ell = E2.Ellipsoid(E2.data, p_test, C_dic)
    # assert (abs(ell.d['T1'] - np.array([881.71, 1352.58, 1584.06])) < 0.01).all(), 'Ellipsoid\
    #        centre incorrect' # TODO
    print(ell.projection(p_test['T1'], p_test['T1'], 'T1'))
    # assert (ell.projection(p_test, p_test, 'T1') == p_test['T1']).all(), 'Bad projection'


def tst_eldp():
    E = Eldp()
    data = E.data

    E2 = Eldp_R()
    data = E2.data
    p_test = {}
    for t in data.T:
        p_test[t] = np.random.random(data.n)

    print(E2.grad(p_test))
    p_test = {}
    for t in data.T:
        p_test[t] = np.asarray(list(E2.data.p_min.values()))
    print(E2.grad(p_test)['T1'].b)
    print(E2.box_constraints(p_test))
    print(E2.ramp_constraints(p_test))
    for t in data.T:
        p_test[t] = np.asarray(list(E2.data.p_min.values()))
    print(E2.box_constraints(p_test))
    print(E2.ramp_constraints(p_test))
    print(E2.data.B_loss)
    # ell = E2.Ellipsoid(E2.data, p_test, C_dic)
    # print('ell.A', ell.A, 'ell.d', ell.d)
    # print(ell.retraction(p_test, p_test))


def test_is_feasible():
    p_test = get_p_test()
    assert (not E2.is_feasible(p_test)), 'This point should be infeasible'
    p_test = None
    assert (not E2.is_feasible(p_test)), 'None point should be infeasible'


def test_is_tight_box():
    p_test = get_p_test()
    S_f = E2.get_fixed_units()
    for t in data.T:
        p_test[t][S_f] = -1
        for i in range(E2.data.n):
            if i not in S_f[0]:
                assert not E2.is_tight_p_max(i, t, p_test),\
                'Point shouldn''t be tight'
                assert not E2.is_tight_p_min(i, t, p_test),\
                'Point shouldn''t be tight'
                p_test[t][i] = data.p_max_np[i]
                assert E2.is_tight_p_max(i, t, p_test),\
                'Point should be tight'
                assert not E2.is_tight_p_min(i, t, p_test),\
                'Point shouldn''t be tight'
                p_test[t][i] = data.p_min_np[i]
                assert not E2.is_tight_p_max(i, t, p_test),\
                'Point shouldn''t be tight'
                assert E2.is_tight_p_min(i, t, p_test),\
                'Point should be tight'


def test_is_tight_ramp():
    p_test = get_p_test()
    # TODO


def test_box_constraints():
    p_test = get_p_test()
    assert E2.box_constraints(p_test), 'This point respect the box'
    p_test['T1'][0] = -1
    p_test['T2'][1] = p_test['T2'][1] * 2
    assert not E2.box_constraints(p_test), 'This point does not respect the box'


def tst_eldp_subgrad():
    E2.param.n_iter = 20
    E2.param.flag_verbose = 0
    solve.eldp_subgrad(E2)


def tst_get_feasible():
    p0 = solve.get_feasible(E2)
    assert E2.is_feasible(p0)


def test_get_fixed_units():
    S_f = E2.get_fixed_units()
    for i in range(data.n):
        if np.isin(i, S_f):
            assert data.p_min_np[i] == data.p_max_np[i], 'Fixed unit'


def tst_get_P_S():
    p_test = solve.get_feasible(E2)
    E2.update(p_test)
    assert solve.get_P_S(E2, p_test).shape == (E2.data.n*len(E2.data.T), E2.data.n),\
        'shape mismatch'
    B = solve.get_P_B(E2, p_test)
    #assert B.shape == (E2.data.n*len(E2.data.T), 4*E2.data.n*len(E2.data.T)), 'shape mismatch'
    b = solve.get_P_b(E2, p_test)


def test_get_S_np():
    S_f = E2.get_fixed_units()
    p_test = get_p_test()
    S1_np = E2.get_S_np(p_test)
    assert np.array_equal(S1_np['T1'], S_f[0]), 'Point should be differentiable'
    assert np.array_equal(S1_np['T2'], S_f[0]), 'Point should be differentiable'

    p_test['T1'][0] = np.array(E2.data.p_min['G1'])
    S2_np = E2.get_S_np(p_test)
    # assert (S2['T1'][0] == np.array([0])).all(), 'Point should not be differentiable in G1'
    assert np.isin(0, S2_np['T1']), 'Point should not be differentiable in G1'
    assert np.array_equal(S1_np['T2'], S_f[0]), 'Point should be differentiable'

    p_test['T2'][1] = np.array(E2.data.p_min['G2'])
    p_test['T1'][2] = np.array(E2.data.p_min['G3'])
    E2.data.gen_fun_param_np[2][3:5] = 1
    print(E2.data.gen_fun_param_np)
    E2.set_S_not_smooth()
    S3_np = E2.get_S_np(p_test)
    print(S3_np)
    assert np.isin([0, 2], S3_np['T1']).all(),\
        'Point should not be differentiable in T1: G1 and G3'
    assert np.isin(1, S3_np['T2']),\
        'Point should not be differentiable in T2: G2'


def test_ramp_constraints():
    p_test = get_p_test()
    assert E2.ramp_constraints(p_test), 'This point respect the ramp'
    p_test['T1'][0:2] = p_test['T1'][0:2] + E2.data.RU_np[0:2]
    p_test['T1'][2] = p_test['T1'][2] - E2.data.RD_np[2]
    assert E2.ramp_constraints(p_test), 'This point respect the ramp'
    p_test = get_p_test()
    p_test['T1'][0:2] = p_test['T1'][0:2] + 2*E2.data.RU_np[0:2]
    print(p_test['T1'])
    print(p_test['T2'])

    assert not E2.ramp_constraints(p_test), 'This point does not respect the ramp'


def tst_sol():
    E2 = Eldp_R()
    data = E2.data
    E2.param.flag_verbose = 1
    E2.param.eps_p = 0.1
    E2.param.step_count_max = 150
    E2.param.step_min = pow(10, -8)
    E2.param.n_iter = 50
    print(E2.data.sum_loads)
    test_sol = getData.get_test_sol()
    print('Solution to test', test_sol)
    E2.update(test_sol)
    test_obj = getData.get_test_obj()
    print(test_sol)
    print(test_sol['T1'])
    assert E2.is_feasible(test_sol), 'This point should be feasible'
    assert abs(E2.f(test_sol)) - test_obj < 0.1
    print(E2.f(test_sol))
    print(E2.get_loss(test_sol))
    print(E2.is_feasible(test_sol))
    raise
 #   E2.param.x0 = getData.get_test_x0()
#    print(E2.f(E2.param.x0), E2.is_feasible(E2.param.x0))
   # solve.eldp_subgrad(E2)
    print('Best _obj', E2.best_obj)
    print(E2.stopping_criterion)
    raise

def test_all_sol():
    str_test_sol = 'test_sol_BBOSB.csv'
    str_test_sol = 'test_sol_ICA.csv'
    str_test_sol = 'test_sol_HIGA.csv'
    str_test_sol = 'test_sol_MILP-IPM.csv'
    E2.param.flag_spinning_reserve = True
    E2.param.flag_verbose = 3
    test_sol = getData.get_test_sol(str_test_sol_sfx=str_test_sol)
    print(E2.f(test_sol))
    print(E2.get_loss(test_sol))
    if E2.param.flag_spinning_reserve:
        print('Reserve', E2.spinning_constraints(test_sol))
    print(E2.is_feasible(test_sol))
    

def tst_sol_TSMILP():
    E2 = Eldp_R()
    E2.param.flag_spinning_reserve = True
    test_sol = getData.get_test_sol('sol_TSMILP_losses.csv')
    test_obj = getData.get_test_obj('obj_TSMILP_losses.csv')
    print(E2.f(test_sol))
    print(E2.get_loss(test_sol))
    if E2.param.flag_spinning_reserve:
        print('Reserve', E2.spinning_constraints(test_sol))
    print(E2.is_feasible(test_sol))
    raise
#    assert E2.is_feasible(test_sol)
    assert abs(E2.f(test_sol) - test_obj) <= 1
    print(E2.f(test_sol))
    E2.param.x0 = test_sol
    E2.best_sr_np = E2.get_sr(test_sol)
    E2.best_sr_10_np = E2.get_sr_10(test_sol)
    E2.param.flag_warm_start = False
    x1 = solve.get_feasible(E2, x=E2.param.x0)
    
    raise

def tst_sol_APLA():
    E2 = Eldp_R()
    param = E2.param
    param.restrict_method = 'local'
    param.flag_warm_start = False
    param.alg = 'APQUA'
    param.n_iter = 1
    getProblem.master(E2, E2.param)
    print('Best obj %s %s' % (param.alg, E2.best_obj))
    print('Best sol %s %s' % (param.alg, E2.full_dic_to_np_dic(E2.best_sol)))
    E2.param.x0 = E2.full_dic_to_np_dic(E2.best_sol)
    param.n_iter = 100
    E2.param.eps_p = 0.1
    param.step_0 = 10
    param.flag_verbose = 1
    solve.eldp_subgrad(E2)
    raise

def test_get_approx():
    tic = time.time()
    print('tic=', tic)
    E2 = Eldp_R()
    E2.param.check_feasibility = False
    E2.param.timeLimit = 60
    E2.param.flag_verbose = 2
    E2.param.flag_OPF = True
    E2.param.flag_spinning_reserve = True
    E2.param.alg_feasible_approx = 'local'
    print(E2.data.B_loss)
    E2.param.x0 = None
    E2.param.x0 = solve.get_approx(E2)
    solve.eldp_subgrad(E2)
    print('Total time is', time.time()-tic)
    raise


def tst_subproblem():
    p_test = solve.get_feasible(E2)
    E2.update(p_test)
    tic = time.time()
    solve.sub_problem(E2, p_test)
    toc = time.time()-tic
    print('Total time for subproblem: %s' % toc)


if __name__ == '__main__':
    global n_f
    n_f = 0
    test_get_approx()
    raise
    tst_sol_TSMILP()
    raise
    test_all_sol()
    raise
    str_test_sol = 'test_sol_HIGA.csv'
    #E2 = Eldp_R()
    #param = E2.param
    #param.x0 = None  #solve.get_approx(E2)
    #p = solve.get_feasible(E2)

#!/usr/bin/env python3

import numpy as np
import os
import matplotlib.pyplot as plt
import tikzplotlib
import matplotlib.patches as patches
from pylab import setp

root_dir, _ = os.path.split(os.path.dirname(os.path.abspath(__file__)))
package_dir = os.path.join(root_dir, 'tmp')
dirs = ['master-full', 'master-local', 'APLA']  # If run with APQUA, switch APLA and APQUA here 
f_names = ['best_obj', 'exec_time', 'opt_gap', 'real_bound']
f_names = ['exec_time', 'opt_gap']
cases = ['case57', 'case118']
fs = 14  # Fontsize
top_frame = 35  # Magnification top (red square)
top_cut = 500  # No data displayed between bottom_cut and top_cut
bottom_cut = 310


def getData():
    data = {(f, d, c): np.array for f in f_names for d in dirs for c in cases}
    for f_name in f_names:
        for directory in dirs:
            for c in cases:
                f = os.path.join(package_dir, directory, c+'_'+f_name+'.txt')
                data[f_name, directory, c] = np.loadtxt(f)
    return data


def plotData1(data):
    fig, ax1 = plt.subplots()
    k = 0
    y_name = ['Execution time (s)', 'Optimality gap (%)']
    f = f_names[1]
    k = 1
    for d in dirs:
        f_data = {c: data[f, d, c] for c in cases}
        bplot1 = ax1.boxplot(f_data.values(),
                            vert=True,  # vertical box alignment
                            positions = np.arange(k, k+len(cases)),
                            widths= 0.6,
                            showmeans=True,
                            patch_artist=True)  # fill with color
        setBoxColors(bplot1)
        ax1.set_ylabel(y_name[1], fontsize=fs)
        k += 1+len(cases)
    ax1.set_xlim(left=0, right=k-1)
    ax1.set_xticklabels(dirs, fontsize=fs)
    ax1.set_xticks([1.5, 4.5, 7.5])
    hB, = plt.plot([0, 0], color='lightblue')
    hR, = plt.plot([0, 0], color='lightgreen')
    ax1.legend((hB, hR), ('Case57', 'Case118'), loc='upper right')
    hB.set_visible(False)
    hR.set_visible(False)
    tikzplotlib.save(os.path.join(root_dir, 'output', 'boxplot_opt_gap.tex'))
    plt.savefig(os.path.join(root_dir, 'output', 'boxplot_opt_gap.eps'))


def plotData2(data):
    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(8, 8), sharex=True)
    y_name = ['Execution time (s)', 'Optimality gap (%)']
    f = f_names[0]
    k = 1
    for d in dirs:
        f_data = {c: data[f, d, c] for c in cases}
        bplot1 = ax1.boxplot(f_data.values(),
                            vert=True,  # vertical box alignment
                            positions = np.arange(k, k+len(cases)),
                            widths= 0.6,
                            showmeans=True,
                            patch_artist=True)  # fill with color
        setBoxColors(bplot1)
        bplot2 = ax2.boxplot(f_data.values(),
                             vert=True,  # vertical box alignment
                             patch_artist=True,  # fill with color
                             widths= 0.6,
                             showmeans=True,
                             positions = np.arange(k, k+len(cases)),
                             )  # will be used to label x-ticks
        setBoxColors(bplot2)
        k += 1+len(cases)
        ax2.set_ylabel(y_name[0], fontsize=fs)
    ax2.yaxis.set_label_coords(-0.08,1.1)
    ax3.yaxis.set_label_coords(-0.08,0.5)

    #zoom
    ax1.set_ylim(bottom=top_cut)  # outliers only
    ax2.set_ylim(top=bottom_cut)  # most of the data
    ax1.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)

    # diagonal

    d = .015  # how big to make the diagonal lines in axes coordinates
    # arguments to pass to plot, just so we don't keep repeating them
    kwargs = dict(transform=ax1.transAxes, color='k', clip_on=False)
    ax1.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
    ax1.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal
    kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
    ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
    ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal

    # draw temporary red and blue lines and use them to create a legend
    hB, = plt.plot([1,1], color='lightblue')
    hR, = plt.plot([1,1], color='lightgreen')
    ax1.legend((hB, hR), ('Case57', 'Case118'))
    hB.set_visible(False)
    hR.set_visible(False)

    ax3.set_ylim(top=top_frame, bottom=0)  # most of the data
    k = 1
    for d in dirs:
        f_data = {c: data[f, d, c] for c in cases}
        bplot3 = ax3.boxplot(f_data.values(),
                             vert=True,  # vertical box alignment
                             patch_artist=True,  # fill with color
                             positions = np.arange(k, k+len(cases)),
                             widths= 0.6,
                             showmeans=True,
                            )  # will be used to label x-ticks
        setBoxColors(bplot3)
        k += 1+len(cases)
    for ax in (ax1, ax2, ax3):
        ax.set_xlim(left = 0, right = k-1)
    ax3.set_xticklabels(dirs, fontsize=fs)
    ax3.set_xticks([1.5, 4.5, 7.5])


    ax3.set_ylabel(y_name[0], fontsize=fs)
    # Create a Rectangle patch
    rect = patches.Rectangle((0.5, 0), 8, top_frame, linewidth=1, edgecolor='r', facecolor='none', zorder=1)  # zorder=10 for rectangle top
    # Add the patch to the Axes
    ax2.add_patch(rect)
    plt.setp([ax1.get_xticklines(), ax.get_yticklines()], color='white')  # remove top tick
    plt.setp(ax3.spines.values(), color='red', linewidth=1)
    tikzplotlib.save(os.path.join(root_dir, 'output', 'boxplot_time.tex'))
    plt.savefig(os.path.join(root_dir, 'output', 'boxplot_time.eps'))


def test_plotData1():
    data = getData()
    plotData1(data)


def test_plotData2():
    data = getData()
    plotData2(data)


def test_getData():
    getData()


# function for setting the colors of the box plots pairs
def setBoxColors(bp):
    colors = ['lightblue', 'lightgreen', 'orange']
    k = 0
    for c in cases:
        setp(bp['boxes'][k], color=colors[k])
        setp(bp['caps'][2*k:2*k+2], color=colors[k])
        setp(bp['whiskers'][2*k:2*k+2], color=colors[k])
        setp(bp['fliers'][2*k:2*k+2], color=colors[k])
        setp(bp['medians'][k], color=colors[2])
        k += 1


def main():
    test_getData()
    test_plotData1()
    test_plotData2()

if __name__ == '__main__':
    main()


import getData
import math
import fun
import numpy as np
import matplotlib.pyplot as plt
import warnings
from gurobipy import *


n_f = 0

class Param:
    def __init__(self, alg='APLA', armijo_coef=0.00001, delta_tol=1, eps_p=pow(10, -4),
                 flag_write=True, n_iter=50, max_step=500, step_0=1, step_count_max=100,
                 step_forward=4, step_min=pow(10, -8), step_scaling=0.5,
                 restrict_method='local', timeLimit=60, x0=None):
        self.alg = alg
        self.alg_feasible_approx = 'local'
        self.armijo_coef = armijo_coef
        self.alpha_cons = 1
        self.alpha_obj = 0
        self.delta_tol = delta_tol
        self.eps_p = eps_p
        self.eps_p_balance = eps_p
        self.eps_cons = 0.01
        self.check_feasibility = True  # Simple sufficient but not necessary check
        self.feasibility_obj = 'MILP'  # 'norm' 'mixed_norm_quadratic' 'MILP' 'losses' 'mixed_losses_quadratic' 'balance_deviation'
        self.feasibility_mixed_cons = 0.1  # mixed objective is: norm + cons * quadratic
        self.flag_local = True
        self.flag_OPF = True
        self.flag_spinning_reserve = False
        self.flag_verbose = 2
        self.flag_warm_start = True
        self.flag_write = False
        self.max_sol = 40
        self.max_step = max_step
        self.mipgap = 0.0001
        self.n_iter = n_iter
        self.restrict_method = restrict_method
        self.step_0 = step_0
        self.step_count_max = step_count_max
        self.step_forward = step_forward
        self.step_min = step_min
        self.step_scaling = step_scaling
        self.timeLimit = timeLimit
        self.x0 = x0


class Eldp:
    def __init__(self, param=Param()):
        B, B_loss, B_loss_0, B_loss_00, bus, bus_generator, bus_load, D, from_bus, gen,\
                gen_fun_param, lines, loads, n, p_max, p_min, RD, RU, SR, T, TC, to_bus\
                = getData.getData()
        self.data = self.Data()
        self._best_bound = 0
        self._best_obj = float('inf')
        self.best_sol = {}
        self.delta = float('inf')
        self.delta_loc = float('inf')
        self.incumbent_num = 0
        self.incumbents_sol = []
        self.isSlave = False
        self.param = param
        self._set_p_max_T()
        self._set_p_min_T()
        self.QP_obj = 0
        self.stopping_criterion = None
        self.step = 1
        self._sur_obj = 0
        self.true_bound = 0
        self._true_obj = float('inf')
        self.true_sol = {}
        self.var = self.Variables()
        self.x_kink = {g: fun.getKinkMid(gen_fun_param[g], p_min[g], p_max[g], 2)
                       for g in self.data.gen_VPE}
        self.xi = {(t, g): fun.getKinkMid(gen_fun_param[g], p_min[g], p_max[g], 2)
                   for t in T for g in self.data.gen_VPE}

    def reset_xi(self):
        self.xi = {(t, g): fun.getKinkMid(self.data.gen_fun_param[g], self.data.p_min[g],
                                          self.data.p_max[g], 2)
                   for t in self.data.T for g in self.data.gen_VPE}

    def bound_tightening(self, mod):
        U = self.sur_obj
        data = self.data
        var = self.var
        count = 0
        mod.addConstr(
            quicksum((data.gen_fun_param[g][0]*var.p[t, g]*var.p[t, g]
                      + data.gen_fun_param[g][1]*var.p[t, g]
                      + data.gen_fun_param[g][2])
                     for t in data.T for g in data.gen) <= U,
            "OptimalityConstr")
        for t in data.T:
            for g in data.gen:
                if self.param.flag_verbose >= 3:
                    print('Tightening bounds for (t, g) = (%s, %s)' % (t, g))
                mod.setObjective(var.p[t, g], GRB.MINIMIZE)
                mod.update()
                mod.optimize()
                mod.write('Upper.lp')
                if mod.status in [2]:
                    self.p_min_T[t, g] = mod.objval
                else:
                    self.p_min_T[t, g] = mod.objbound
                mod.setObjective(var.p[t, g], GRB.MAXIMIZE)
                mod.update()
                mod.optimize()
                if abs(self.p_max_T[t, g] - mod.objval) > 1:
                    count += 1
                if mod.status in [2]:
                    self.p_max_T[t, g] = mod.objval
                else:
                    self.p_max_T[t, g] = mod.objbound
        return mod


    def is_tight_p_min(self, i, t, x):
        return abs(x[t][i]-self.data.p_min_np[i]) <= self.param.eps_p

    def is_tight_p_max(self, i, t, x):
        return abs(x[t][i]-self.data.p_max_np[i]) <= self.param.eps_p

    def is_tight_RU(self, i, t, x):
        assert self.data.T_dic[t] > 0
        t1 = self.data.T[self.data.T_dic[t]-1]
        t2 = t
        return abs(x[t2][i] - x[t1][i] - self.data.RU_np[i]) <= self.param.eps_p

    def is_tight_RD(self, i, t, x):
        assert self.data.T_dic[t] > 0
        t1 = self.data.T[self.data.T_dic[t]-1]
        t2 = t
        return abs(- self.data.RD_np[i] - (x[t2][i] - x[t1][i])) <= self.param.eps_p

    def is_tight_S_p_max(self, i, t, p, sr):
        return abs(sr[t][i] - self.data.p_max_np[i] + p[t][i]) <= self.param.eps_p

    def is_tight_S_RU(self, i, t, sr):
        return abs(sr[t][i] - self.data.RU_np[i]) <= self.param.eps_p

    def is_tight_S_60(self, t, x):
        return abs(np.sum(np.minimum(self.data.p_max_np-x[t], self.data.RU_np)
                   - self.data.SR[t])) <= self.param.eps_p

    def is_tight_S_balance(self, t, sr):
        return abs(sum(sr[t])-self.data.SR[t]) <= self.param.eps_p_balance

    def is_tight(self, i, t, x):
        out = self.is_tight_p_min(i, t, x) or self.is_tight_p_max(i, t, x)
        if t != self.data.T[0]:
            out = out or self.is_tight_RD(i, t, x) or self.is_tight_RU(i, t, x)

        return out

    def printBest(self):
        print("Surrogate objective", self.sur_obj)
        print("True objective", self.true_obj)
        print("Best true objective is", self.best_obj)
        print("Best bound is", self.best_bound)
        print("Delta is", self.delta)
        print("Delta local is", self.delta_loc)

    def plotFun(self, t, g):
        try:
            data = self.data
            p_space = np.linspace(data.p_min[g], data.p_max[g], 100)
            plt.plot(p_space, self.f(p_space, t, g))
            plt.show()
        except TypeError:
            print('Input argument should be string')

    def _get_best_obj(self):
        return self._best_obj

    def _set_best_obj(self, val):
        self._best_obj = val
        # Update delta when best_obj is updated
        self.delta = self._best_obj - self.best_bound

    best_obj = property(_get_best_obj, _set_best_obj)

    def _get_true_obj(self):
        return self._true_obj

    def _set_true_obj(self, val):
        self._true_obj = val
        # Update delta when best_obj is updated
        self.delta_loc = self._true_obj - self.sur_obj

    true_obj = property(_get_true_obj, _set_true_obj)

    def _get_best_bound(self):
        return self._best_bound

    def _set_best_bound(self, val):
        self._best_bound = val
        # Update delta when best_bound is updated
        self.delta = self.best_obj - self._best_bound

    best_bound = property(_get_best_bound, _set_best_bound)

    def _get_p_max_T(self):
        return self.p_max_T

    def _set_p_max_T(self):
        self.p_max_T = {(t, g): self.data.p_max[g] for (t, g) in self.data.gen_T}

    def _set_p_min_T(self):
        self.p_min_T = {(t, g): self.data.p_min[g] for (t, g) in self.data.gen_T}

    def _get_sur_obj(self):
        return self._sur_obj

    def _set_sur_obj(self, val):
        self._sur_obj = val
        # Update delta when best_bound is updated
        self.delta_loc = self.true_obj - self._sur_obj

    sur_obj = property(_get_sur_obj, _set_sur_obj)

    def f(self, x, *args):  # args = t, g
        global n_f
        n_f += 1
        if len(args) == 0:
            cumSum = 0
            for t in self.data.T:
                for i in range(self.data.n):
                    if type(x) is dict:
                        cumSum += self.f(x[t][i], t, self.data.gen[i])
                    else:
                        cumSum += self.f(x[t, self.data.gen[i]], t, self.data.gen[i])
            return cumSum
        elif len(args) == 2:
            t = args[0]
            g = args[1]
            return fun.f(x, self.data.gen_fun_param[g], self.data.p_min[g])
        else:
            raise ValueError('Unexpected arguments', args)

    def f_VPE_part(self, x):
        global n_f
        n_f += 1
        obj_VPE = 0
        for t in self.data.T:
            for g in self.data.gen_VPE:
                obj_VPE += fun.f_VPE_part(x[t, g], self.data.gen_fun_param[g],
                                          self.data.p_min[g])
        return obj_VPE

    def f_VPE(self, x):
        obj_VPE = 0
        global n_f
        n_f += 1
        for t in self.data.T:
            for g in self.data.gen_VPE:
                obj_VPE += self.f(x[t, g], t, g)
        return obj_VPE

    def full_dic_to_np_dic(self, x):
        data = self.data
        out = {t: np.zeros(data.n) for t in data.T}
        for (t, g) in data.gen_T:
            out[t][data.gen_dic[g]] = x[t, g]
        return out

    def gurobi_sol_to_np(self, x):
        data = self.data
        return {t: np.array([x[t, data.gen[i]] for i in range(data.n)]) for t in data.T}

    def update_best_bound(self, val):
        self.best_bound = max(self.best_bound, val)

    def update_best_sol(self, sol, forced=False):
        sol_obj = self.f(sol)
        sol_np = self.gurobi_sol_to_np(sol)

        if forced or self.best_obj == float('Inf'):
            self.best_sol = sol
            self.best_obj = sol_obj
            if self.param.flag_spinning_reserve:
                self.best_sr_np = self.get_sr(self.gurobi_sol_to_np(sol))
                self.best_sr_10_np = self.get_sr_10(self.gurobi_sol_to_np(sol))
            return

        dev_sol = self.balance(sol_np)
        dev_best_sol = self.balance(self.gurobi_sol_to_np(self.best_sol))
        bi_objective_sol = self.param.alpha_obj*sol_obj/self.best_obj\
            + self.param.alpha_cons*dev_sol/dev_best_sol
        if dev_best_sol < self.param.eps_p:
            bi_objective_sol = sol_obj/self.best_obj
        if bi_objective_sol <= 1:
            print('Bi objective', bi_objective_sol)
            print('Best obj =%s and current obj = %s' % (self.best_obj, sol_obj))
            print('Best dev =%s and current dev = %s' % (dev_best_sol, dev_sol))
            print('Updating best solution to closer one from %s to %s',
                  (self.best_obj, sol_obj))
            self.best_sol = sol
            self.best_obj = sol_obj
            if self.param.flag_spinning_reserve:
                self.best_sr_np = self.get_sr(self.gurobi_sol_to_np(sol))
                self.best_sr_10_np = self.get_sr_10(self.gurobi_sol_to_np(sol))

    class Variables:
        def __init__(self):
            self.f = None
            self.p = None
            self.p_VPE = None

    class Data:
        def __init__(self):
            B, B_loss, B_loss_0, B_loss_00, bus, bus_generator, bus_load, D, from_bus, gen,\
                    gen_fun_param, lines, loads, n, p_max, p_min, RD, RU, SR, T, TC, to_bus\
                    = getData.getData()
            self.B = B  # Conductance [dict]
            self.B_loss = B_loss  # Matrix losses [numpy array n x n]
            self.B_loss_0 = B_loss_0  # Vector losses [numpy array n x 1]
            self.B_loss_00 = B_loss_00  # Scalar losses [scalar]
            self.bus = bus  # Buses of the network [list]
            self.bus_generator = bus_generator  # Generator location []
            self.bus_load = bus_load  # Load location []
            self.bus_T = [(t, b) for t in T for b in bus]
            self.D = D  # Demand []
            self.from_bus = from_bus  # Buses reachable from given bus []
            self.gen = gen  # gen [SET]
            self.gen_dic = {gen[i]: i for i in np.arange(n)}
            self.gen_np = np.array(gen)  # gen [SET]
            self.gen_fun_param = gen_fun_param  # param of generators
            self.gen_fun_param_np = self.to_np(gen_fun_param)  # param of generators
            self.gen_T = [(t, g) for t in T for g in gen]
            self._set_gen_VPE()
            self.gen_VPE_T = [(t, g) for t in T for g in self.gen_VPE]
            self.lines = lines
            self.lines_T = [(t, l) for t in T for l in lines]
            self.loads = loads
            self.n = n
            self.n_fun_param = len(gen_fun_param[gen[0]])
            self.p_max = p_max
            self.p_max_np = self.to_np(p_max)
            self.p_min = p_min
            self.p_min_np = self.to_np(p_min)
            self.RD = RD  # Down ramp rate
            self.RD_np = self.to_np(RD)
            self.RU = RU
            self.RU_np = self.to_np(RU)
            self.SR = SR
            self.SR_10 = {t: SR[t]*2.0/6.0 for t in T}
            self.T = T
            self.T_dic = {T[t]: t for t in np.arange(len(T))}
            self.TC = TC
            self._set_TC_max()
            self.TC_max_T = {(t, l): self.TC_max[l] for (t, l) in self.lines_T}
            self.TC_min = {l: -self.TC_max[l] for l in self.lines}
            self.TC_min_T = {(t, l): self.TC_min[l] for (t, l) in self.lines_T}
            self.T_gen = {(t, g) for t in T for g in gen}
            self.to_bus = to_bus

            self._set_D_tot()  # _set_D_tot needs self.T
            self.sum_loads = {t: sum(self.D_tot[t, b] for b in self.bus)
                              for t in self.T}
            self.D_full = np.zeros((len(self.T), 1))
            i = 0
            for (t, b) in self.bus_T:
                self.D_full[self.T_dic[t]] += self.D_tot[t, b]

        def to_np(self, d):
            return np.array(list(d.values()))

        def _set_gen_VPE(self):
            self.gen_VPE = []
            for g in self.gen:
                if self.gen_fun_param[g][4] != 0 or self.gen_fun_param[g][3] != 0:
                    self.gen_VPE.append(g)

        def _set_D_tot(self):
            self.D_tot = {}
            # Sum all load to get the load at each bus
            for b in self.bus:
                for t in self.T:
                    _dic = {t: self.D[(t, l)] for l in self.loads if self.bus_load[l] == b}
                    self.D_tot[(t, b)] = sum(_dic.values())

        def _set_TC_max(self):
            for l in self.lines:
                if self.TC[l] == 0:
                    self.TC_max[l] = float('inf')


class Eldp_R(Eldp):
    def __init__(self):
        super().__init__()
        self.ell = self.Ellipsoid(self.data, None)
        self.par = self._get_par()
        self.gen_num = {self.data.gen[i]: i for i in range(self.data.n)}
        self.gen_num_np = self.data.to_np(self.gen_num)
        self.get_this_S_np = self.get_S_np
        self.S = {t: np.array([]) for t in self.data.T}
        self.S_not_smooth = self.get_S_not_smooth()
        self.S_np = {t: np.array([]) for t in self.data.T}

    def balance(self, x):
        dev = 0
        for t in self.data.T:
            dev += abs(x[t].T @ self.ell.A_full[t] @ x[t] + x[t].T @ self.ell.b_full[t]
                       + self.ell.c_full[t])
        if self.param.flag_verbose >= 1:
            print('Balance deviation is', dev)
        return dev

    def box_constraints(self, x):
        flag_box = True
        data = self.data
        param = self.param
        count = 0
        for t in data.T:
            _flag_box = np.logical_and(x[t] >= data.p_min_np - param.eps_cons,
                                       x[t] <= data.p_max_np + param.eps_cons)
            flag_box = flag_box and _flag_box.all()
            if not _flag_box.all():
                err_box = np.where(np.invert(_flag_box))
                count += len(err_box[0])
                if param.flag_verbose <= 1:
                    # a single constraint has been detected
                    return flag_box
                else:
                    print('Box violated in times %s and units' % t, err_box[0])
        if param.flag_verbose == 2:
            print('Number of violated box constrai_nnts = %d \n' % count)
        return flag_box

    def spinning_constraints(self, p):

        data = self.data
        sr = self.get_sr(p)
        sr_10 = self.get_sr_10(p)
        for t in data.T:
            if np.any(sr[t] - data.p_max_np + p[t] >= self.param.eps_p) or\
               np.any(sr_10[t] - data.p_max_np + p[t] >= self.param.eps_p) or\
               np.any(sr[t] - data.RU_np >= self.param.eps_p) or\
               np.any(sr_10[t] - data.RU_np/6 >= self.param.eps_p) or\
               np.sum(sr[t]) - data.SR[t] <= -self.param.eps_p or\
               np.sum(sr_10[t]) - data.SR_10[t] <= -self.param.eps_p:
                if self.param.flag_verbose >=3:
                    print('T is', t)
                    print('Reserve sum', np.sum(sr[t]), data.SR[t])
                    print('Reserve sum 10', np.sum(sr_10[t]), data.SR_10[t])
                    print('Reserve RU', sr[t]-data.RU_np)
                    print('Reserve RU 10', sr_10[t]-data.RU_np/6)
                    print('Reserve Pmax', sr[t]-data.p_max_np + p[t])
                    print('Reserve Pmax 10', sr_10[t]-data.p_max_np + p[t])
                return False
        if self.param.flag_verbose >= 1:
            print('No reserve constraint violated')
        return True

    def get_sr(self, p):
        data = self.data
        sr = {t: np.zeros(data.n) for t in data.T}
        for t in data.T:
            sr[t] = np.minimum(data.p_max_np - p[t], data.RU_np)
        return sr

    def get_sr_10(self, p):
        data = self.data
        sr = {t: np.zeros(data.n) for t in data.T}
        for t in data.T:
            sr[t] = np.minimum(data.p_max_np - p[t], data.RU_np/6)
        return sr

    def is_feasible(self, x):
        feasible = (x is not None) and self.ramp_constraints(x) and self.box_constraints(x) \
                and self.balance(x) < 0.001
        if self.param.flag_spinning_reserve and feasible:
            feasible = self.spinning_constraints(x)
            print('Spinning constraints =', feasible)
        return feasible

    def get_fixed_units(self):
        data = self.data
        return np.where(data.p_min_np == data.p_max_np)

    def get_loss_t(self, t, x):
        data = self.data
        loss = x.T @ data.B_loss @ x + np.dot(data.B_loss_0, x) + data.B_loss_00
        if self.param.flag_verbose >= 2:
            print('Loss for time t = %s are %s' % (t, loss))
        return loss

    def get_loss(self, x):
        loss = 0
        for t in self.data.T:
            loss += self.get_loss_t(t, x[t])
        return loss

    def get_S_np(self, x):
        S = {}
        data = self.data
        param = self.param
        for t in data.T:
            S[t] = (np.abs(
                np.mod(
                    x[t][self.S_not_smooth] - data.p_min_np[self.S_not_smooth], math.pi/data.gen_fun_param_np[self.S_not_smooth, 4]
                )
            ) < param.eps_p).nonzero()
            S[t] = S[t][0]
        return S

    def get_S_not_smooth(self):
        S_not_smooth = np.logical_not(np.logical_or(self.data.gen_fun_param_np[:, 3] == 0.,
                                 self.data.gen_fun_param_np[:, 4] == 0.)).nonzero()
        return S_not_smooth[0]

    def get_S_tight_np(self, x):
        data = self.data
        S_tight = self.get_S_np(x)

        for t in data.T:
            for i in range(self.data.n):
                if self.is_tight(i, t, x):
                    S_tight[t] = np.union1d(S_tight[t], i)
                elif t != data.T[-1]:
                    next_t = data.T[1+data.T_dic[t]]
                    if self.is_tight_RD(i, next_t, x) or self.is_tight_RU(i, next_t, x):
                        S_tight[t] = np.union1d(S_tight[t], i)
                        S_tight[next_t] = np.union1d(S_tight[next_t], i)
                if (t, i) == ('T6', 0):
                    print('Check here')
                    print(x[t][i])
                    print(x['T5'][i])
                    print(S_tight[t])
        return S_tight

    def get_S_direction_np(self, x, direction):
        data = self.data
        S_tight = self.get_S_np(x)
        direction = np.reshape(direction, (len(data.T), data.n))
        for t in data.T:
            t1 = data.T_dic[t]
            t2 = data.T_dic[t]+1
            for i in range(self.data.n):
                if data.p_min_np[i] > x[t][i] and direction[t1, i] < 0:
                    S_tight[t] = np.union1d(S_tight[t], i)
                elif data.p_max_np[i] < x[t][i] and direction[t1, i] > 0:
                    S_tight[t] = np.union1d(S_tight[t], i)

                elif t != data.T[-1]:
                    next_t = data.T[1+data.T_dic[t]]
                    if data.RD_np[i] > x[t][i] - x[next_t][i] and (direction[t1, i] > direction[t2, i]):
                        S_tight[t] = np.union1d(S_tight[t], i)
                        S_tight[next_t] = np.union1d(S_tight[next_t], i)
                    if x[next_t][i] - x[t][i] > data.RU_np[i] and (direction[t1, i] < direction[t2, i]):
                        S_tight[t] = np.union1d(S_tight[t], i)
                        S_tight[next_t] = np.union1d(S_tight[next_t], i)
        return S_tight

    def set_S(self):
        data = self.data
        for t in data.T:
            if self.S_np[t].size > 0:
                self.S[t] = self.data.gen_np[self.S_np[t]]

    def set_S_not_smooth(self):
        self.S_not_smooth = self.get_S_not_smooth()

    def set_S_np(self, p):
        self.S_np = self.get_this_S_np(p)

    def grad(self, x):
        data = self.data
        df = {}
        for t in data.T:
            (df_quad, _df_VPE) = fun.df(x[t], self.par, list(data.p_min.values()))
            if self.S_np[t].size > 0:  # Singular indices in x, compute generalized subgradient
                sg_f = self.Subgrad()
                # Differentiable component of the sine term
                D = np.setdiff1d(self.gen_num_np, self.S_np[t], assume_unique=True)  # Tocheck
                df_VPE = np.zeros_like(x[t])
                for i in D:
                    df_VPE[i] = _df_VPE[i]
                sg_f.b = df_VPE + df_quad
                # Variable elements in the generalized subgradient
                sg_f.A = np.zeros((data.n, data.n))
                sg_f.A[:, self.S_np[t]] = fun.sg_f(x[t], self.par, self.S_np[t])
                df[t] = sg_f

            else:  # No singular index in x, subgradient = gradient
                df[t] = df_quad + _df_VPE
        return df

    def ramp_constraints(self, x):
        flag_ramp = True
        data = self.data
        param = self.param
        count = 0
        for t in range(1, len(data.T)):
            X1 = x[data.T[t]]
            X0 = x[data.T[t-1]]
            _flag_ramp = np.logical_and(X1-X0-data.RU_np <= param.eps_cons,
                                        - param.eps_cons <= data.RD_np + X1-X0)
            flag_ramp = flag_ramp and _flag_ramp.all()
            if not _flag_ramp.all():
                err_ramp = np.where(np.invert(_flag_ramp))
                count += len(err_ramp[0])
                if param.flag_verbose <= 1:
                    # a single constraint has been detected
                    return flag_ramp
                else:
                    print('Ramp violated between times %s and %s for units' %
                          (data.T[t-1], data.T[t]), err_ramp[0])
                    print('RU = %s and RD = %s' % (data.RU_np[err_ramp[0]],
                                                   data.RD_np[err_ramp[0]]))
                    print(x[data.T[t-1]][err_ramp[0]], x[data.T[t]][err_ramp[0]])
        if param.flag_verbose == 2:
            print('Number of violated ramp constraints = %d \n' % count)
        return flag_ramp

    def update(self, p, S=None):
        if S is None:
            self.set_S_np(p)
        else:
            self.S_np = S
        self.set_S()
        self.ell.C_dic = self.S_np
        self.ell.update_ellipsoid(p, self.S_np)

    def _get_par(self):
        data = self.data
        par = []
        for i in range(data.n_fun_param):
            par.append(np.zeros(data.n))
            for g in range(data.n):
                par[i][g] = data.gen_fun_param[data.gen[g]][i]
        return par

    class Subgrad:
        def __init__(self):
            self.A_sg = {}
            self.b_sg = {}

    class Ellipsoid:
        def __init__(self, data, x, C_dic=None):
            # C_dic contains the index (numeral) of the gen
            self.C_dic = C_dic
            self.data = data
            _A, _b, _c, _d = self.get_ellipsoid(x)
            self.A_full = _A
            self.b_full = _b
            self.c_full = _c
            self.d_full = _d
            self.update_ellipsoid(x, C_dic)

        def check_ellipsoid(self, x, t):
            return abs(x.T @ self.A[t] @ x + self.b[t].T @ x + self.c[t]) < 0.1

        def projection(self, x, v, t=None):
            """ Compute the projection at p of the vector v on T_p:
                P_x(v) = xi
            """
            A = self.data.B_loss
            b = self.data.B_loss_0-1
            if t is None:
                nn = {}
                xi = {}
                for t in self.data.T:
                    nn[t] = 2*A @ x[t] + b
                    nn[t] = nn[t]/np.linalg.norm(nn[t])
                    xi[t] = (np.eye(self.data.n)-nn[t] @ nn[t].T) @ v[t]
            else:
                nn = 2*A @ x + b
                nn = nn.reshape(nn.shape[0], 1)
                nn = nn/np.linalg.norm(nn)
                xi = (np.eye(self.data.n) - nn @ nn.T) @ v
            return xi

        def retraction(self, x_dic, xi_dic):
            if self.d[self.data.T[0]] is None:
                warnings.warn("Retraction on flat ellipsoid")
                return xi_dic
            data = self.data
            q = {}
            for t in data.T:
                x = np.array(x_dic[t])
                q[t] = np.zeros_like(x)
                xi = np.array(xi_dic[t])
                C = np.array(self.C_dic[t], dtype='int64')
                F = np.arange(data.n)
                if self.C_dic[t].size == data.n:
                    q[t] = x
                    continue
                if self.C_dic[t].size > 0:
                    F = np.setdiff1d(F, C)
                w = x[F] + xi[F] - self.d[t]
                a1 = w.T @ self.A[t] @ w
                a2 = 2*self.d[t].T @ self.A[t] @ w + w.T @ self.b[t]
                a3 = self.d[t].T @ (self.A[t] @ self.d[t]+self.b[t]) + self.c[t]
                delta = a2**2 - 4*a1*a3
                if delta < 0:
                    raise ValueError('Negative delta, it seems impossible to find intersections\
                                     between (sub-)ellipsoid and the line between x and centre')
                betas = (-a2 + np.array([1, -1]).T * np.sqrt(delta).T) / (2.0*a1)
                if a1 == 0: # TO DEAL WITH
                    raise ValueError('This should not happend')

                ind = np.argmin(np.abs(betas-1))
                q[t][C] = x[C]
                q[t][F] = self.d[t] + betas[ind]*w
            return q
        
        def retraction_not_feasible(self, x_dic, xi_dic):
            if self.d[self.data.T[0]] is None:
                warnings.warn("Retraction on flat ellipsoid")
                return xi_dic
            data = self.data
            q = {}
            A = data.B_loss
            b = data.B_loss_0-1
            d_old = np.linalg.solve(A, -b/2.0)
            for t in data.T:
                x = np.array(x_dic[t])
                q[t] = np.zeros_like(x)
                xi = np.array(xi_dic[t])
                C = np.array(self.C_dic[t], dtype='int64')
                F = np.arange(data.n)
                if self.C_dic[t].size == data.n:
                    q[t] = x
                w = x - d_old
                a1 = w.T @ A @ w
                a2 = 2*d_old.T @ A @ w + w.T @ b
                a3 = d_old.T @ (A @ d_old + b) + self.data.B_loss_00 + self.data.sum_loads[t]
                delta = a2**2 - 4*a1*a3
                if delta < 0:
                    raise ValueError('Negative delta, it seems impossible to find intersections\
                                     between (sub-)ellipsoid and the line between x and centre')
                betas = (-a2 + np.array([1, -1]).T * np.sqrt(delta).T) / (2.0*a1)
                if a1 == 0: # TO DEAL WITH
                    raise ValueError('This should not happened')

                ind = np.argmin(np.abs(betas-1))
                print('step is %s' % betas[ind])
                q[t] = d_old + betas[ind]*w
            return q

        def get_ellipsoid(self, x, C_dic=None):
            print('Updating ellipsoid')
            data = self.data
            if C_dic is None:
                C_dic = {t: np.array([]) for t in data.T}
            out_A = {}
            out_b = {}
            out_c = {}
            out_d = {}
            A = data.B_loss
            b = data.B_loss_0-1
            try:
                d_old = np.linalg.solve(A, -b/2.0)
            except np.linalg.LinAlgError:
                print(np.linalg.LinAlgError('The system is lossless, ellipsoid is a plane.'))
                d_old = None
            except ValueError:
                raise ValueError('Are you sure A and b have the same dimension ? A.shape = %s and b.shape = %s' % (A.shape, b.shape))

            for t in data.T:
                A = data.B_loss
                b = data.B_loss_0-1
                c = data.B_loss_00 + data.sum_loads[t]
                d = d_old
                if C_dic[t].size > 0:
                    C = np.array(C_dic[t])
                    p = np.array(x[t])
                    F = np.setdiff1d(np.arange(0, data.n), C, assume_unique=True)
                    _A = A[np.ix_(F, F)]
                    _b = 2*A[np.ix_(F, C)] @ p[C] + b[F]
                    _c = p[C].T @ (b[C]+A[np.ix_(C, C)] @ p[C])+c
                    A = _A
                    b = _b
                    c = _c
                    if d_old is not None:
                        d = np.linalg.solve(A, -b/2.0)
                out_A[t] = A
                out_b[t] = b
                out_c[t] = c
                out_d[t] = d
            return out_A, out_b, out_c, out_d

        def update_ellipsoid(self, x, C_dic=None):
            _A, _b, _c, _d = self.get_ellipsoid(x, C_dic)
            self.A = _A
            self.b = _b
            self.c = _c
            self.d = _d
